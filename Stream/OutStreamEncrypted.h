#pragma once
#include "OutStream.h"
#include <string.h>

class OutStreamEncrypted : public OutStream
{
private:
	int _shift;
public:
	OutStreamEncrypted(int shift);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *f));
	OutStreamEncrypted& operator<<(int num);
};