#include "Logger.h"
#include <string.h>
void Logger::setStartLine(bool value)
{
	static unsigned int lineNum = 0; 
	
	if (_startLine == true)
	{//start a new line and add  to line counter
		lineNum++; 
		fprintf(stdout, "%s", "LOG ");
		fprintf(stdout, "%d: ", lineNum);
	}
	
	this->_startLine = value;
}

Logger::Logger(): _startLine(true)
{

}

Logger::~Logger()
{

}

Logger & operator<<(Logger & l, const char * msg)
{
	if (l._startLine == true) 
	{
		l.setStartLine(false); 
	}
	const char* str = strstr(msg, "\n");
	
	if (str != NULL) //if \n in msg
	{
		for (int i = 0; i < str - msg; i++)
		{
			fprintf(stdout, "%c", msg[i]); //prints msg
		}
		
		l << endline;
		l.setStartLine(false);
		fprintf(stdout, "%s", str + 2);
		
	}
	else
	{
		fprintf(stdout, "%s", msg);
	}
	
	return l;
}

Logger & operator<<(Logger & l, int num)
{
	if (l._startLine == true)
	{
		l.setStartLine(false);
	}
	fprintf(stdout, "%d", num); //prints num to screen
	return l;
}

Logger & operator<<(Logger & l, void(*pf)(FILE* f))
{
	l.setStartLine(true); //changes start line to true, after end
	pf(stdout);
	return l;
}
