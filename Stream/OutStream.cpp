#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(stdout,"%s", str); //prints string to screen
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(stdout,"%d", num); //prints number to screen
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* f))
{
	pf(stdout); //calls end line function
	return *this;
}




void endline(FILE* f)
{
	fprintf(f, "\n"); //prints ens line
}
