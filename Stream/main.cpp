
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"


int main(int argc, char **argv)
{
	OutStream out;
	out << "I am the Doctor and I�m " << 1500 << " years old" << endline;
	
	FileStream file("doctor.txt");
	file << "I am the Doctor and I�m " << 1500 << " years old" << endline;

	OutStreamEncrypted out1(4);
	out1 << "I am the Doctor and I�m "  << 1500 << " years old" << endline;
	

	Logger log, car;
	log << "I am the Doctor\n and I�m " << 1500 << " years old" << endline;
	log << "I know a lot" << endline;
	car << "I saved " << 3400 << "people" << endline;
	
	getchar();
	return 0;
}
