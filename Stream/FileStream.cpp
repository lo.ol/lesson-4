#include "FileStream.h"

#pragma warning(disable : 4996)

FileStream::FileStream(char * filePath)
{
	this->_file = fopen(filePath, "a"); //creates a file 
}

FileStream::~FileStream()
{
	fclose(this->_file); 
}

FileStream& FileStream::operator<<(const char * str)
{
	fprintf(this->_file, "%s",str); //prints string to file
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	fprintf(this->_file, "%d",num); //prints number to file
	return *this;
}

FileStream & FileStream::operator<<(void(*pf)(FILE *f))
{
	pf(_file);
	return *this;
}


