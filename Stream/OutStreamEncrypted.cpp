#include "OutStreamEncrypted.h"
#pragma warning(disable : 4996)

OutStreamEncrypted::OutStreamEncrypted(int shift) : _shift(shift)
{
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(const char * str)
{
	char ch;
	//loop encryptes string
	for (int i = 0; i < strlen(str); i++)
	{ 
		if (str[i] >= ' ' && str[i] <= '~') //checks if char in range
		{
			ch = str[i] + _shift; 
			if (ch > '~') //in case char not in range after shift was added
			{
				ch = ch - '~' + ' ' - 1; 
			}
		}
		fprintf(stdout, "%c", ch);
	}
	
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(void(*pf)(FILE *f))
{
	pf(stdout);
	return *this;
}

OutStreamEncrypted & OutStreamEncrypted::operator<<(int num)
{
	int digits = 0, numCopy = num;
	while (numCopy != 0) //loop finds number of digits in num
	{
		numCopy /= 10;
		++digits; 
	}
	char* str = new char[digits + 1]; //creates a str to turn num to str
	sprintf(str, "%d", num); //put num to str as str
	*this << str; //prints num, sends it to <<(str) function
	delete[] str; //delete str
	return *this;
}
